#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#ifdef WIN32
	#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
	#include <time.h>
#else
	#include <unistd.h>
#endif
/**	----- DATA STORAGE -----
	*
	* A byte is an unsigned char
	* and is represented as 7654 3210
	*
	* - Bit 0 (1 = alive | 0 = dead): current cell state
	* - Bit 1 (2 | 0): previous cell state
	*
	* Values therefore range
	* from 0 up to 3
	*
	* This storage method allows calculation, update, and display
	* in the same loop, reducing complexity from 3n to n
	* ----- ------------ ----- */

/** ----- -------- ----- */
/** ----- TYPEDEFS ----- */
/** ----- -------- ----- */

typedef unsigned char byte;

/** ----- ------------------- ----- */
/** ----- FUNCTION PROTOTYPES ----- */
/** ----- ------------------- ----- */

int getUInt(char* printed);
void clearSTDIN();
void clearSTDOUT();
void resetCursorSTDOUT();
void wait(unsigned int millisec);

void run(void);
void nextFrame(byte* board, int width, int height);
byte* getRandomBoard(int size); // malloc and init a random board
void cleanup(byte* board); // cleanup memory


int getIndex(int x, int y, int width); // return 1 dimension index from 2 dimensions indexes
int getNeighbours(byte* board, int x, int y, int width, int height); // sum up live neighbours of cell (x, y)
void applyRules(byte* board, int idx, int neighbours); // apply rules to cell at index idx

/** ----- ---- ----- */
/** ----- MAIN ----- */
/** ----- ---- ----- */

int main()
{
	run();

	return 0;
}

/** ----- ------------------------ ----- */
/** ----- FUNCTION IMPLEMENTATIONS ----- */
/** ----- ------------------------ ----- */

void wait(unsigned int millisec)
{
	#ifdef WIN32
		Sleep(millisec);
	#elif _POSIX_C_SOURCE >= 199309L
		struct timespec ts;
		ts.tv_sec = (int) (millisec / 1000);
		ts.tv_nsec = (millisec % 1000) * 1000000;
		nanosleep(&ts, NULL);
	#else
		// uses nanoseconds
		usleep(millisec * 1000);
	#endif
}

int getUInt(char* printed)
{
	int i;

	printf("%s", printed);
	while (scanf("%d", &i) != 1)
	{
		clearSTDIN();
		printf("%s", printed);
	}
	return i;
}

void clearSTDIN()
{
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}

void clearSTDOUT()
{
	#ifdef WIN32
		HANDLE                     hStdOut;
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		DWORD                      count;
		DWORD                      cellCount;
		COORD                      homeCoords = { 0, 0 };

		hStdOut = GetStdHandle( STD_OUTPUT_HANDLE );
		if (hStdOut == INVALID_HANDLE_VALUE) return;

		/* Get the number of cells in the current buffer */
		if (!GetConsoleScreenBufferInfo( hStdOut, &csbi )) return;
		cellCount = csbi.dwSize.X *csbi.dwSize.Y;

		/* Fill the entire buffer with spaces */
		if (!FillConsoleOutputCharacter(
			hStdOut,
			(TCHAR) ' ',
			cellCount,
			homeCoords,
			&count
			)) return;

		/* Fill the entire buffer with the current colors and attributes */
		if (!FillConsoleOutputAttribute(
			hStdOut,
			csbi.wAttributes,
			cellCount,
			homeCoords,
			&count
			)) return;

		/* Move the cursor home */
		SetConsoleCursorPosition(hStdOut, homeCoords);
	#else
		printf("\033[2J")
	#endif
}

void resetCursorSTDOUT()
{
	#ifdef WIN32
		HANDLE                     hStdOut;
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		COORD                      homeCoords = { 0, 0 };

		hStdOut = GetStdHandle( STD_OUTPUT_HANDLE );

		if (hStdOut == INVALID_HANDLE_VALUE) return;
		if (!GetConsoleScreenBufferInfo( hStdOut, &csbi )) return;

		SetConsoleCursorPosition(hStdOut, homeCoords);
	#else
		printf("\033[H");
	#endif
}

void cleanup(byte* board)
{
	free(board);
}

byte* getRandomBoard(int size)
{
	// seed random
	srand(time(NULL));
	// allocate (size squared) bytes
	byte* board = malloc(size * sizeof(byte));

	for (int i = 0; i < size; i++)
		board[i] = rand() % 2;

	return board;
}

int getIndex(int x, int y, int width)
{
	return y * width + x;
}

int getNeighbours(byte* board, int x, int y, int width, int height)
{
	int neighbours = 0;
	// init loop bounds to avoid outOfRange error
	int xstart = (x == 0) ? x : x - 1;
	int xend = (x == width - 1) ? x : x + 1;

	int ystart = (y == 0) ? y : y - 1;
	int yend = (y == height - 1) ? y : y + 1;

	for (int i = ystart; i <= yend; i++)
	{
		for (int j = xstart; j <= xend; j++)
		{
			int idx = getIndex(j, i, width);

			if (x == j && y == i)
				// "current neighbour" is actually cell (x, y) -> pass
				continue;
			if (i == y - 1 || (i == y && j == x - 1))
				// current neighbour is already updated, check previous state
				neighbours += ((board[idx] & 0b10) >> 1) & 1;
			else
				// current neighbour has not been updated, check current state
				neighbours += board[idx] & 1;
		}
	}
	return neighbours;
}

void applyRules(byte* board, int idx, int neighbours)
{
	// alive && (neighbours < 2 || neighbours > 3) => 0
	// dead && neighbours == 3 => 1
	// else prev => current
	if ((board[idx] & 0b10) && (neighbours < 2 || neighbours > 3))
		board[idx] &= 0b10;
	else if (!(board[idx] & 0b10) && neighbours == 3)
		board[idx] |= 1;
	else
		// reset bit 0 then duplicate value of bit 1
		board[idx] = (board[idx] & 0b10) | ((board[idx] >> 1) & 1);
}

void nextFrame(byte* board, int width, int height)
{
	for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int idx = getIndex(x, y, width);
				// previous state = current state
				// bit shifting discard current state, which will be set next
				board[idx] <<= 1;

				// sums up neighbours
				int neighbours = getNeighbours(board, x, y, width, height);
				// apply the rules to current state
				applyRules(board, idx, neighbours);
				// display current state
				printf("%c", (board[idx] & 1) ? 219 : 176);
			}
			printf("\n");
		}
}

void run()
{
	// init variables
	int width = getUInt("Please enter the board's width: ");
	int height = getUInt("Please enter the board's height: ");

	byte* board = getRandomBoard(width * height);

	clearSTDOUT();
	// loop
	while (true)
	{
		resetCursorSTDOUT();
		nextFrame(board, width, height);
		wait(200);
	}
	// last line should be cleanup call
	cleanup(board);
}
